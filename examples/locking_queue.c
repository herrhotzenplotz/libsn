#include "locking_queue.h"

#include <stdlib.h>

/* WARNING! THIS CODE IS A PURE HACK!
 * DO NOT BLAME OR SHOUT AT ME. I AM NOT RESPONSIBLE FOR IT!
 */

static void
lock_queue(struct generic_lqueue *q)
{
    if (pthread_mutex_lock(&q->lock))
        abort();
}

static void
unlock_queue(struct generic_lqueue *q)
{
    if (pthread_mutex_unlock(&q->lock))
        abort();
}

void  lqueue_init(struct generic_lqueue *lqueue)
{
    lqueue->head=lqueue->tail=NULL;
    lqueue->size=0;

    if (pthread_mutex_init(&lqueue->lock, NULL))
        abort();
}
void  lqueue_push_front(struct generic_lqueue *lqueue,void *elem)
{
    struct lqueue_entry *entry=malloc(sizeof(struct lqueue_entry));
    entry->elem=elem;
    entry->prev=NULL;

    lock_queue(lqueue);
    entry->next=lqueue->head;
    if(!lqueue->head)
    {
        lqueue->head=lqueue->tail=entry;
    }
    else
    {
        lqueue->head->prev=entry;
        lqueue->head=entry;
    }

    lqueue->size++;

    unlock_queue(lqueue);
}
void  lqueue_push_back(struct  generic_lqueue  *lqueue,void *elem)
{
    struct lqueue_entry *entry = malloc(sizeof(struct lqueue_entry));
    entry->elem=elem;

    lock_queue(lqueue);
    entry->prev=lqueue->tail;
    entry->next=NULL;
    if(!lqueue->tail)
    {
        lqueue->head=lqueue->tail=entry;
    }
    else
    {
        lqueue->tail->next=entry;
        lqueue->tail=entry;
    }
    lqueue->size++;
    unlock_queue(lqueue);
}

void* lqueue_pop_front(struct generic_lqueue *lqueue)
{
    lock_queue(lqueue);
    if(lqueue->head)
    {
        struct lqueue_entry *entry=lqueue->head;
        void *ret=entry->elem;

        if(lqueue->head->next)
        {
            lqueue->head=lqueue->head->next;
            lqueue->head->prev=NULL;
        }
        else
        {
            lqueue->head=lqueue->tail=NULL;
        }
        lqueue->size--;
        unlock_queue(lqueue);

        free(entry);
        return ret;
    }

    unlock_queue(lqueue);
    return NULL;
}

void* lqueue_pop_back(struct generic_lqueue *lqueue)
{
    lock_queue(lqueue);
    if(lqueue->tail)
    {
        struct lqueue_entry *entry=lqueue->tail;
        void *ret=entry->elem;
        free(entry);
        lqueue->tail=lqueue->tail->prev;
        lqueue->tail->next=NULL;
        lqueue->size--;
        unlock_queue(lqueue);
        return ret;
    }
    unlock_queue(lqueue);
    return NULL;
}
//returning value in case it needs to be freed
void* lqueue_remove(struct generic_lqueue *lqueue, struct lqueue_entry *entry)
{
    lock_queue(lqueue);

    if(entry->prev)
        entry->prev->next=entry->next;
    if(entry->next)
        entry->next->prev=entry->prev;
    //if(entry==lqueue->head)
    //	lqueue->head=entry->next;
    //if(entry==lqueue->tail)
    //	lqueue->tail=entry->prev;
    void *value=entry->elem;
    free(entry);
    lqueue->size--;
    if(lqueue->size==0)
    {
        lqueue->head=lqueue->tail=NULL;
    }

    unlock_queue(lqueue);

    return value;
}

//To compare  objects
int   lqueue_find(struct generic_lqueue *lqueue, void *elem,
                  int (*cmpfunc)(void *elem1,void *elem2))
{
    lock_queue(lqueue);
    if(!elem)
    {
        unlock_queue(lqueue);
        return -1;
    }
    struct lqueue_entry *entry=lqueue->head;
    while(entry)
    {
        if(cmpfunc(entry->elem,elem)==0)
        {
            unlock_queue(lqueue);
            return 0;
        }
        entry=entry->next;
    }
    unlock_queue(lqueue);
    return 1;

}

int   lqueue_destroy(struct generic_lqueue *lqueue)
{
    lock_queue(lqueue);
    struct lqueue_entry *elem=lqueue->head;
    while(elem)
    {
        struct lqueue_entry *tmp=elem;
        elem=elem->next;
        free(tmp);
    }
    unlock_queue(lqueue);
    return 0;
}


void lqueue_cpy(struct generic_lqueue *dst,struct generic_lqueue *src)
{
    lock_queue(dst);
    lock_queue(src);
    //sanity checks
    if(src->head && dst->tail)
    {
        //Link src head to dst tail->next
        dst->tail->next=src->head;
        //new tail is now in src tail
        dst->tail=src->tail;
        //free ptr of src
        free(src);
        src=NULL;
    }
    unlock_queue(dst);
    unlock_queue(src);
}

void* lqueue_get_idx(struct generic_lqueue *lqueue, size_t idx)
{
    lock_queue(lqueue);
    size_t count=0;
    struct lqueue_entry *elem=lqueue->head;
    while(elem)
    {
        if(count==idx)
            return unlock_queue(lqueue), elem->elem;
        elem=elem->next;
        count++;
    }
    return unlock_queue(lqueue), NULL;
}
