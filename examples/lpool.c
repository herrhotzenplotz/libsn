/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "lpool.h"
#include <string.h>

#ifdef __sun
#include <sched.h>
#define pthread_yield() sched_yield()
#endif

#ifdef __linux__
#define pthread_yield() sched_yield()
#endif

static void *
lpool_worker(void *_data)
{
    struct lpool *pool = _data;

    void *item = NULL;

    while (!pool->done) {
        item = lqueue_pop_front(&pool->queue);
        if (item)
            pool->kernel(item);
        else
            pthread_yield();

        free(item);
    }

    return NULL;
}

int
lpool_init(struct lpool *pool, size_t threads, size_t item_size, void (*kernel)(void *))
{
    lqueue_init(&pool->queue);

    pool->threads_size = threads;
    pool->item_size    = item_size;
    pool->threads      = calloc(sizeof(pthread_t), threads);
    pool->kernel       = kernel;

    for (size_t i = 0; i < threads; ++i)
        if (pthread_create(&pool->threads[i], NULL, lpool_worker, pool))
            return -1;

    return 0;
}

void
lpool_submit_item(struct lpool *pool, void *item, size_t size)
{
    void *cpy = malloc(size);
    memcpy(cpy, item, size);

    lqueue_push_back(&pool->queue, cpy);
}

int
lpool_finalize(struct lpool *pool)
{
    pool->done = true;

    for (size_t i = 0; i < pool->threads_size; ++i)
        if (pthread_join(pool->threads[i], NULL))
            return -1;

    free(pool->threads);
    return 0;
}
