/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __linux__
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <math.h>
#include <unistd.h>

#include <sn/sn.h>
#include <sn/q.h>

#include "lpool.h"

struct work_item {
    double  x_min, x_max;
    size_t  n;
    int     i;
    int     verbose;
    double *output_buffer;

    double (*function)(double);
};

static inline double
dx(struct work_item *item)
{
    return (item->x_max - item->x_min) / (double)(item->n);
}

static inline double
derivative(struct work_item *item)
{
    double x0 = item->x_min + ((double)(item->i) * dx(item));
    double x1 = x0 + dx(item);
    return (item->function(x1) - item->function(x0)) / dx(item);
}

static void
kernel(void *_data)
{
    struct work_item *item = _data;

    if (item->verbose)
        fprintf(stderr, "     : [kernel] : picking up item %d\n", item->i);

    item->output_buffer[item->i] = derivative(item);
}

int
main(int argc, char *argv[])
{
    int             error     = 0;
    int             n         = 500000;
    double          x_min     = 0.0;
    double          x_max     = 2.0 * M_PI;
    double          max_error = 0;
    size_t          threads   = 6;
    double          lockfree_diff, locking_diff;
    int             verbose   = 0;
    struct timespec timer_begin, timer_end;
    int             ch;

    while ((ch = getopt(argc, argv, "vn:l:h:t:")) != -1) {
        switch (ch) {
        case 'n':
            n = strtol(optarg, NULL, 10);
            break;
        case 'l':
            x_min = strtod(optarg, NULL);
            break;
        case 'h':
            x_max = strtod(optarg, NULL);
            break;
        case 't':
            threads = strtol(optarg, NULL, 10);
            break;
        case 'v':
            verbose = 1;
            break;
        case '?':
        default:
            errx(1, "usage: %s [-v] [-t threads] [-n steps] [-l lower_x] [-h higher_x]", argv[0]);
        }
    }
    argc -= optind;
    argv += optind;

    fprintf(stderr, "INFO : Verbose mode is turned %s\n",
            verbose ? "on" : "off (use -v to turn on)");

    {
        sn_q queue                     = {0};
        struct work_item lockfree_item = {
            .output_buffer = calloc(sizeof(double), n),
            .verbose       = verbose,
            .n             = n,
            .x_min         = x_min,
            .x_max         = x_max,
            .function      = sin,
        };

        fprintf(stderr, "INFO : Lockfree : Starting computation\n");

        /* Start the threads */
        if ((error = sn_q_init(&queue, threads, sizeof(struct work_item), kernel)))
            errx(1, "sn_q_init failed with code %d", error);

        /* Start the timer */
        if (clock_gettime(CLOCK_MONOTONIC, &timer_begin) < 0)
            err(1, "clock_gettime");

        /* Submit work items to the threads */
        for (int i = 0; i < n; ++i) {
            if (verbose)
                fprintf(stderr, "     : [main]   : enqueueing item %d\n", i);

            lockfree_item.i = i;
            sn_q_nq(&queue, &lockfree_item);
        }

        /* Wait for the treads to finish the computation */
        if ((error = sn_q_finalize_and_destroy(&queue)))
            errx(1, "sn_q_finalize_and_destroy failed with code %d", error);

        /* Stop the timer */
        if (clock_gettime(CLOCK_MONOTONIC, &timer_end) < 0)
            err(1, "clock_gettime");

        lockfree_diff = (timer_end.tv_sec - timer_begin.tv_sec) + (double)(timer_end.tv_nsec - timer_begin.tv_nsec) / (double)1e9;
    }

    {
        struct lpool     queue        = {0};
        struct work_item locking_item = {
            .output_buffer = calloc(sizeof(double), n),
            .verbose       = verbose,
            .n             = n,
            .x_min         = x_min,
            .x_max         = x_max,
            .function      = sin,
        };

        fprintf(stderr, "INFO : Locking  : Starting computation\n");

        /* Start the threads */
        if ((error = lpool_init(&queue, threads, sizeof(struct work_item), kernel)))
            errx(1, "sn_q_init failed with code %d", error);

        /* Start the timer */
        if (clock_gettime(CLOCK_MONOTONIC, &timer_begin) < 0)
            err(1, "clock_gettime");

        /* Submit work items to the threads */
        for (int i = 0; i < n; ++i) {
            locking_item.i = i;
            if (verbose)
                fprintf(stderr, "     : [main]   : enqueueing item %d\n", i);

            lpool_submit_item(&queue, &locking_item, sizeof(struct work_item));
        }

        /* Wait for the treads to finish the computation */
        if ((error = lpool_finalize(&queue)))
            errx(1, "sn_q_finalize_and_destroy failed with code %d", error);

        /* Stop the timer */
        if (clock_gettime(CLOCK_MONOTONIC, &timer_end) < 0)
            err(1, "clock_gettime");

        locking_diff = (timer_end.tv_sec - timer_begin.tv_sec) + (double)(timer_end.tv_nsec - timer_begin.tv_nsec) / (double)1e9;

        fprintf(stderr, "INFO : sanity-checking output data\n");
        for (int i = 0; i < n-1; ++i) {
            double d_x        = (x_max - x_min) / (double)(n);
            double x          = x_min + (double)(i) * d_x;
            double y          = cos(x);
            double curr_error = fabs(y - locking_item.output_buffer[i]);

            if (curr_error > max_error)
                max_error = curr_error;
        }
    }

    fprintf(stderr, "INFO : Results\n");
    fprintf(stderr, "     : Execution time - lockfree          : %-15.9lf seconds\n", lockfree_diff);
    fprintf(stderr, "     : Execution time - locking           : %-15.9lf seconds\n", locking_diff);
    fprintf(stderr, "     : #Threads                  (-t)     : %zu\n", threads);
    fprintf(stderr, "     : #Steps                    (-n)     : %d\n", n);
    fprintf(stderr, "     : Lower derivative boundary (-l)     : %12.9lf\n", x_min);
    fprintf(stderr, "     : Upper derivative boundary (-h)     : %12.9lf\n", x_max);
    fprintf(stderr, "     : Maximum numerical derivative error : %e\n", max_error);

    return 0;
}
