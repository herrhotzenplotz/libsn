// Kindly donated by Charilaos Skandylas

#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include <pthread.h>

struct lqueue_entry
{
    void *elem;
    struct lqueue_entry *next,*prev;
};

//a lqueue of lqueue_entries
struct generic_lqueue
{
    pthread_mutex_t lock;
    size_t size;
    struct lqueue_entry *head,*tail;
};



void  lqueue_init(struct generic_lqueue *lqueue);
void  lqueue_push_front(struct generic_lqueue *lqueue,void *elem);
void  lqueue_push_back(struct  generic_lqueue  *lqueue,void *elem);
//ownership transfers
void* lqueue_pop_front(struct generic_lqueue *lqueue);
//ownership transfers
void* lqueue_pop_back(struct generic_lqueue *lqueue);
void* lqueue_remove(struct generic_lqueue *lqueue, struct lqueue_entry *entry);
//for complex objects
int   lqueue_find(struct generic_lqueue *lqueue, void *elem,
                 int (*cmpfunc)(void *elem1,void *elem2));
int   lqueue_destroy(struct generic_lqueue *lqueue);

//copy one lqueue to another
void lqueue_cpy(struct generic_lqueue *dst,struct generic_lqueue *src);
void lqueue_push_back_entry(struct generic_lqueue *lqueue, struct lqueue_entry *entry);

//get a specific element
void* lqueue_get_idx(struct generic_lqueue *lqueue, size_t idx);

#endif
