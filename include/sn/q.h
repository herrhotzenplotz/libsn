/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef Q_H
#define Q_H

#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>

#include <sn/sn.h>

typedef struct q sn_q;

struct q {
    void             *items;
    size_t            items_capacity, item_size;
    _Atomic(size_t)   read_idx;
    _Atomic(size_t)   write_idx;
    _Atomic(size_t)   maxread_idx;
    void             (*kernel)(void *);
    pthread_t       *thread_pool;
    size_t           thread_pool_size;
    _Atomic(bool)    done;
};

enum {
    SNQE_OK = 0,
    SNQE_CRTTHR,
    SNQE_NOMEM,
    SNQE_JOINTHR,
};

int  sn_q_init(sn_q *q, size_t threads, size_t item_size, void (*kernel)(void *));
void sn_q_nq(sn_q *q, void *);
int  sn_q_finalize_and_destroy(sn_q *q);

#endif /* Q_H */
