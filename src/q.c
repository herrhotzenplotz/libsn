/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sn/q.h>

#include <string.h>

#ifdef __sun
#include <sched.h>
#define pthread_yield() sched_yield()
#endif

#ifdef __GLIBC__
#define pthread_yield() sched_yield()
#endif

static int
sn_q_dq(sn_q *q, void **out)
{
    size_t next_read, curr_read, curr_maxread;

top:
    do {
        curr_read    = atomic_load(&q->read_idx);
        curr_maxread = atomic_load(&q->maxread_idx);

        if (curr_read == curr_maxread) {
            if (atomic_load(&q->done))
                return 1;

            goto top;
        }

        *out = q->items + curr_read * q->item_size;

        next_read = (curr_read + 1) % q->items_capacity;
    } while (!atomic_compare_exchange_weak(&q->read_idx, &curr_read, next_read));

    return 0;
}

static void *
sn_q_worker_task(void *_data)
{
    sn_q *q         = _data;
    void *work_item = NULL;

    while (!sn_q_dq(q, &work_item))
        q->kernel(work_item);

    return NULL;
}

int
sn_q_init(sn_q *q, size_t threads, size_t item_size, void (*kernel)(void *))
{
    q->item_size        = item_size;
    q->thread_pool_size = threads;
    q->items_capacity   = 10;
    q->items            = calloc(item_size, q->items_capacity);
    q->read_idx         = ATOMIC_VAR_INIT(0);
    q->write_idx        = ATOMIC_VAR_INIT(0);
    q->maxread_idx      = ATOMIC_VAR_INIT(0);
    q->thread_pool      = calloc(sizeof(pthread_t), q->thread_pool_size);
    q->kernel           = kernel;

    if (!q->items)
        return SNQE_NOMEM;

    if (!q->thread_pool)
        return free(q->items), SNQE_NOMEM;

    for (size_t i = 0; i < threads; ++i) {
        if (pthread_create(&q->thread_pool[i], NULL, sn_q_worker_task, q))
            return SNQE_CRTTHR;
    }

    return SNQE_OK;
}

int
sn_q_finalize_and_destroy(sn_q *q)
{
    atomic_store(&q->done, true);

    // Join all the threads
    for (size_t i = 0; i < q->thread_pool_size; ++i)
        if (pthread_join(q->thread_pool[i], NULL))
            return SNQE_JOINTHR;

    // Destroy all the buffers now that all threads are dead
    free(q->thread_pool);
    free(q->items);

    return SNQE_OK;
}

void
sn_q_nq(sn_q *q, void *data)
{
    size_t curr_write, curr_read, next_write;

top:
    do {
        curr_write = atomic_load(&q->write_idx);
        curr_read  = atomic_load(&q->read_idx);
        next_write = (curr_write + 1) % q->items_capacity;

        // queue is full
        if (next_write == curr_read)
            goto top;

    } while (!atomic_compare_exchange_weak(&q->write_idx, &curr_write, next_write));
    // If the atomic cmpxchg fails, this means that another producer
    // wrote. Thus try again.

    // Enqueue item and advance queue
    memcpy(q->items + q->item_size * curr_write, data, q->item_size);

    while (!atomic_compare_exchange_weak(&q->maxread_idx, &curr_write, next_write))
        pthread_yield();
}
