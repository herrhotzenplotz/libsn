# Declare the list of programs
PROGS				=	queue
LIBS				=	libsn.a
INSTALL_HEADERS			=	include/sn/sn.h \
					include/sn/q.h

# These and LDFLAGS can be overwritten
CFLAGS				=	-std=c11 \
					-Iinclude/

# List the source files for each binary to be built
libsn.a_SRCS			=	src/sn.c \
					src/q.c
queue_SRCS			=	examples/queue.c \
					examples/locking_queue.c \
					examples/lpool.c
CPPFLAGS_sparc-sunos-sunstudio	=	-DEXTENSIONS
CFLAGS_sparc-sunos-sunstudio	=	-xatomic=studio
LDFLAGS_sparc-sunos-sunstudio	=	-lrt

queue_LDFLAGS			=	-L. -lsn -lm -lpthread

# Leave this undefined if you don't have any manpages that need to be
# installed.
#MAN				=	docs/ghcli.1

# Include the rules to build your program
# Important: the autodetect.sh script needs to be in place
include default.mk
